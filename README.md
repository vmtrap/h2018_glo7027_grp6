# README

Voici quelques explications sur les scripts que nous avons utilisés, ainsi que les méthodes pour les exécuter


## Structure

Chaque partie du projet (tests et script) est ajoutée dans un dossier séparé. Les données fournies pas Kaggle sont déposées dans le répertoire `data`.

Une liste complète est présente ci-après, et un fichier `README` est présent dans chacun des sous-répertoires. Nous utilisons le format `Markdown` pour simplifier le formattage des fichiers d'explication.

Concernant les fichiers projet, les scripts Python prennent les fichiers venant du répertoire `data` et produisent des sorties vers le répertoire `output` de la partie du projet correspondante (`p1_count_element/output`, `p2p2_data_exploration/output`, etc).


## Liste des scripts et tests

Répertoire|Contenu
--|--
`/data`|Données du projet fournies par Kaggle
`/elastic`|Procédure d'installation d'une instance d'Elasticsearch et Kibana pour visualiser les données
`/test_fausse_soumission`|Test de soumission pour le projet Kaggle, simplement pour voir le type de rendu attendu sur le site
`/p1_compte_element`|Test de comptage du nombre d'éléments dans les données
`/p2_data_exploration`|Divers scripts pour l'exploration de données
`/p3_algorithm`|Application des scripts prévus et primo evaluation
`/p4_final_state`|Retour sur p3 et amélioration de l'analyse


## Notes

Deux langages ont étés utilisés ici. Nous le justifions par la répartition des tâches entre Régis Ongaro-Carcy et Gwenaëlle Lemoine qui avaient chacun des facilités avec respectivement python et R.
Par ailleurs, si python est plus efficace pour le traitement rapide des données, notamment pour les méthodes de machine learning avec scikit learn, R est plus efficace pour la réalisation de statistiques exploratoire de façon simple.
