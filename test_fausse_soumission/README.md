# README

Simple script R pour tester la soumission de réponse sur Kaggle.

Étapes :

1. Récupère le fichier d'entrainement et le fichier de test
2. Identifie les différentes catégories culinaire
3. Génère une réponse aléatoire de catégorie pour chaque id du jeu de test
4. Enrigistre le tout au format csv
