
# Elasticsearch + Kibana instance

Install jdk 1.8

[Oracle](http://www.oracle.com/technetwork/java/javase/downloads/index.html)

Download:

- [Elasticsearch](https://www.elastic.co/downloads/elasticsearch)
- [Kibana](https://www.elastic.co/downloads/kibana)

> Notes
>
> Elasticsearch evolves very rapidly, I recommend to download version ES > 6.0.0, as it removes the sub-index "type" value (deprecated, really removed in ES > 7) and add some neat features for kibana.

Create one folder to contain them on your disk, say `elastic_bowl`

Move them into this folder and decompress them

In CLI:

- go to the `elastic_bowl` folder, then `./bin/elasticsearch`
- then, in another terminal, same folder `./bin/kibana`

Check if the processes are alive with `ps -ef | grep elastic` and `ps -ef | grep kibana`

Now, chromium or another browser, then [here](http://localhost:5601)


# Upload data

With Python3
`pip install elasticsearch`

Push data on elasticsearch:

> Prerequisites
>
> 1. `elasticsearch` and `kibana` running
>
> 2. the `train.json` extracted from `data` folder must be placed in this folder

In CLI, execute `python3 elastic_push.py`

# Script

see the other file in this folder `elastic_push.py`
