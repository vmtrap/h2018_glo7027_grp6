#!/usr/bin/env python3
# -*- coding: utf-8 -*-

__version__ = '1.0.0'
__author__ = 'R. Ongaro'
__date__ = '31-01-2018'

import os.path
import json
from tqdm import tqdm
from elasticsearch import Elasticsearch, helpers

def delete_index(client=None, index=None, **kwargs):
    """Delete an index """
    if index is None:
        raise TypeError
    client.indices.delete(index, **kwargs)

def create_index(client=None, index: str=None, index_type: str=None, mappings: dict=None, settings: dict=None, **kwargs):
    """Create an index """
    if index is None:
        raise TypeError
    kwargs['index'] = index
    body = {}
    disable_all = {"_all": {"enabled": False}}
    # mapping
    used_type = index_type if index_type is not None else index
    if mappings is None:
        mappings = disable_all
    else:
        mappings.update(disable_all)
    body['mappings'] = {'{}'.format(used_type): mappings, }
    # settings
    if settings is not None:
        body = {} if body is None else body
        body['settings'] = settings
    # add the new options to the body
    kwargs['body'] = body
    return client.indices.create(**kwargs)


def send_bulk(client=None, index: str=None, index_type: str=None, dataset=None):
    """Send a bulk"""
    action = [{
        "_op_type": 'index',
        "_index": index,
        "_type": index_type,
        "_source": d
    } for d in dataset]
    for ok, item in helpers.streaming_bulk(client, action, chunk_size=1000, raise_on_error=False, request_timeout=30):
        # go through request-reponse pairs and detect failures
        if not ok: raise Exception('Error during data sending')

def gen_dataset(filepath):
    """Generator from file"""
    with open(filepath, 'r') as f:
        yield from json.load(f, encoding='utf-8')

if __name__ == "__main__":

    # attributes for elasticsearch client
    _hosts = ["localhost:9200"]
    _es = Elasticsearch(hosts=_hosts, retry_on_timeout=True, use_ssl=False)

    # settings for ES, because we only have one instance
    es_settings = {
        "number_of_shards": 1,
        "number_of_replicas": 0
    }
    _NAME = 'cooking'
    es_index_name = _NAME
    es_index_type = _NAME

    # delete index
    print('Deleting index "{}": '.format(es_index_name), end='')
    try:
        delete_index(client=_es, index=es_index_name, ignore=[400, 404])
        print('done')
    except:
        print('fail')

    # create index
    print('Creating index "{}": '.format(es_index_name), end='')
    try:
        create_index(_es, index=es_index_name, index_type=es_index_type, settings=es_settings)
        print('done')
    except:
        print('fail')

    # load dataset from file
    dataset = None
    filepath = os.path.join(os.path.dirname(__file__), 'train.json')
    dataset = gen_dataset(filepath)

    # # test
    # for i in gen_dataset(filepath):
    #     print(i)

    # push data into ES
    print('Sending data... ')
    try:
        send_bulk(client=_es, index=es_index_name, index_type=es_index_type, dataset=dataset)
        print('done')
    except:
        print('fail')
