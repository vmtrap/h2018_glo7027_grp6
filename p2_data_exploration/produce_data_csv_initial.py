
import csv
import json
import os.path
from tqdm import tqdm

def get_data():
    """Yield each cooking informations"""
    d = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), 'data', 'train.json')
    with open(d) as f:
        yield from json.load(f, encoding='utf-8')

def get_output_dir():
    output_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'output')
    if not os.path.isdir(output_dir):
        os.mkdir(output_dir)
    return output_dir

if __name__ == "__main__":

    # output folder
    output_dir = get_output_dir()
    get_output_filepath = lambda x: os.path.join(output_dir, x)

    # unique_ingredients = list(sorted(set([transform(v) for f in get_data() for v in f['ingredients']])))
    unique_ingredients = sorted(list(set([v for f in get_data() for v in f['ingredients']])))

    # in file
    def test_presence(unique_ingr, ingredients):
        '''return a list that match a presence or absence position in unique ingredient list'''
        return [1 if i in ingredients else 0 for i in unique_ingr]

    # produce file
    with open(get_output_filepath('initial_data.csv'), 'w') as f:
        writer = csv.writer(f, delimiter=';', quotechar='|', quoting=csv.QUOTE_MINIMAL)
        # header
        writer.writerow(['id', 'cuisine'] + unique_ingredients)
        # data
        for d in tqdm(list(get_data())):
            writer.writerow([d['id'], d['cuisine']] + test_presence(unique_ingredients, d['ingredients']))
