
import os.path
import json
import csv
from tqdm import tqdm

class Foodb:
    """FooDB: represents the FooDB database

    Useful to search food names and features
    """

    def __init__(self, filepath):
        self._path = filepath
        self._data = self._build(filepath)

    def _build(self, filepath):
        """build foodb data"""
        with open(filepath, 'r', encoding='ISO-8859-1') as csvfile:
            keys_select = ['food_group', 'food_subgroup', 'category', 'name', 'name_scientific']
            reader = csv.DictReader(csvfile, delimiter=',')
            return [{ k:r[k].lower() for k in keys_select } for r in reader]

    def search(self, s):
        """search a name in foodb foods name"""
        res = None
        for f in self._data:
            if f['name'] == s:
                res = f
                break
        return res

def get_data():
    """Yield each cooking informations"""
    d = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), 'data', 'train.json')
    with open(d) as f:
        yield from json.load(f, encoding='utf-8')

def get_output_dir():
    output_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'output')
    if not os.path.isdir(output_dir):
        os.mkdir(output_dir)
    return output_dir


if __name__ == "__main__":

    # output folder
    output_dir = get_output_dir()
    get_output_filepath = lambda x: os.path.join(output_dir, x)

    # only recipe with less than 20 ingredients
    data = [e for e in get_data() if len(e['ingredients']) <= 20]

    # sorted list of unique ingredients
    unique_ingredients = sorted(list(set([v for f in get_data() for v in f['ingredients']])))

    # build foodb
    d = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), 'data', 'foods.csv')
    foodb = Foodb(d)

    # test integration
    good = 0
    wrong = 0
    total = 0
    for u in tqdm(unique_ingredients):
        if foodb.search(u) is None:
            wrong += 1
        else:
            good += 1
        total += 1

    # print
    print('\nuniques ingredients: {}'.format(len(unique_ingredients)))
    assert total == len(unique_ingredients)
    print('{} not found ({:.3f}%)'.format(wrong, (wrong/total)*100))
    print('{} found ({:.3f}%)'.format(good, (good/total)*100))

    # because we can
    print('Result: ', end='')
    print('>:(' if wrong > good else ':D')
