#!/usr/bin/env python3
# -*- coding: utf-8 -*

import csv
import json
import os.path

import numpy as np
import statistics as st
import matplotlib.pyplot as plt
import matplotlib.cbook as cbook


class Stats:
    """Simple statistical printing class

    Represents statistical values print values and boxplot in file or to screen
    """

    _TENDANCE_SYM = 'symétrique'
    _TENDANCE_ASYM_POS = 'asymétrique positive'
    _TENDANCE_ASYM_NEG = 'asymétrique négative'

    def __init__(self, values, percent_step=5):
        self.values = values
        self._max = max(values)
        self._min = min(values)
        self._mean = st.mean(values)
        self._median = st.median(values)
        self._median_low = st.median_low(values)
        self._median_high = st.median_high(values)
        self._mode = st.mode(values)
        self._range = max(values) - min(values)
        # Population standard deviation of data.
        self._pstdev = st.pstdev(values)
        # Population variance of data.
        self._pvariance = st.pvariance(values)
        # Sample standard deviation of data.
        self._stdev = st.stdev(values)
        # Sample variance of data.
        self._variance = st.variance(values)
        # tendance
        if st.mode(values) < st.median(values):
            self._tendance = Stats._TENDANCE_ASYM_POS
        elif st.mode(values) > st.median(values):
            self._tendance = Stats._TENDANCE_ASYM_NEG
        else:
            self._tendance = Stats._TENDANCE_SYM
        # percentile
        self._data_array = np.array(list(values)) # input array
        percentiles = np.percentile(self._data_array, np.arange(0, 100, percent_step)) # quartiles
        self._percentiles = {i*percent_step:percentiles[i] for i,p in enumerate(list(percentiles))}

    def __str__(self):
        msg = 'mesure,value\n'
        msg += 'max,{:.3f}\n'.format(self._max)
        msg += 'min,{:.3f}\n'.format(self._min)
        msg += 'mean,{:.3f}\n'.format(self._mean)
        msg += 'median,{:.3f}\n'.format(self._median)
        msg += 'median_low,{:.3f}\n'.format(self._median_low)
        msg += 'median_high,{:.3f}\n'.format(self._median_high)
        msg += 'mode,{:.3f}\n'.format(self._mode)
        msg += 'tendance,{}\n'.format(self._tendance)
        msg += 'range,{}\n'.format(self._range)
        msg += 'pstdev,{:.3f}\n'.format(self._pstdev)
        msg += 'pvariance,{:.3f}\n'.format(self._pvariance)
        msg += 'stdev,{:.3f}\n'.format(self._stdev)
        msg += 'variance,{:.3f}\n'.format(self._variance)
        for i,v in sorted(self._percentiles.items()):
            msg += 'percentile {},{}\n'.format(i,v)
        return msg[:-1]

    def save(self, to_file=None):
        """Save values to file"""
        if to_file is None:
            raise ValueError('Needed a file path')
        with open(to_file, 'w') as f:
            writer = csv.writer(f, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
            for r in str(self).split('\n'):
                writer.writerow(r.split(','))

    def boxplot(self, to_file=None, log=False):
        """Create boxplot with values"""
        _, ax = plt.subplots()
        ax.set_title('Data boxplot')
        ax.boxplot(self._data_array)
        if log:
            plt.yscale('log')
            plt.autoscale(True)
        ax.set_ylabel('number of ingredients')
        if to_file is None:
            plt.show()
        else:
            plt.savefig(to_file)


def get_data():
    """Yield each cooking informations"""
    d = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), 'data', 'train.json')
    with open(d) as f:
        yield from json.load(f, encoding='utf-8')

def get_output_dir():
    """Get output directory path"""
    output_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'output')
    if not os.path.isdir(output_dir):
        os.mkdir(output_dir)
    return output_dir




if __name__ == "__main__":

    """
    Ici, on s'intéresse aux ingrédients.
    """

    # output folder
    output_dir = get_output_dir()
    get_output_filepath = lambda x: os.path.join(output_dir, x)

    # -------------------------------------------
    # complete dataset

    # dict data
    # format = {id:number of ingredients}
    complete_dataset = {e['id']:len(e['ingredients']) for e in get_data()}

    # stats all data
    complete_stats = Stats(complete_dataset.values())
    complete_stats.save(to_file=get_output_filepath('nb_ingredients.csv'))
    complete_stats.boxplot(to_file=get_output_filepath('boxplot_nb_ingredients.png'))

    # print
    print('Number of all recipes: {}'.format(len(complete_dataset)))

    # -------------------------------------------
    # remove recipes with >20 and <=2 # of ingredients

    # our condition, e is one recipe object
    condition = lambda e: len(e['ingredients']) <= 20 and len(e['ingredients']) > 2

    # select smaller_dataset
    smaller_dataset = {
        e['id']:len(e['ingredients'])
        for e in get_data()
        if condition(e)
    }

    # Stat this
    sup20_stats = Stats(smaller_dataset.values())
    sup20_stats.save(to_file=get_output_filepath('nb_ingredients_without_sup20.csv'))
    sup20_stats.boxplot(to_file=get_output_filepath('boxplot_nb_ingredients_without_sup20.png'))

    # print
    print('Number of all recipes with >20 and <2 ingredients: {}'.format(len(smaller_dataset)))


    # -------------------------------------------
    # Make some ratio on removed data

    # some more var to make some removed data lookup after that
    nb = 0 # number of removed values
    smaller_dataset = {} # dataset with extreme values removed
    removed_by_cuisine = {} # dataset with the number of recipe removed by cuisine type
    total = {}

    # select smaller_dataset
    for e in get_data():
        # if less than 20 ingredients and more than 2 in the recipe, keep it
        if condition(e):
            smaller_dataset[e['id']] = len(e['ingredients'])

        else:
            # incr nb of removed values
            nb += 1

            # total removed count per cuisine type
            try:
                removed_by_cuisine[e['cuisine']] += 1
            except KeyError:
                removed_by_cuisine[e['cuisine']] = 1

        # total per cuisine type
        try:
            total[e['cuisine']] += 1
        except KeyError:
            total[e['cuisine']] = 1

    ratio_dict = [{'cuisine':k, 'remove':v, 'total':total[k], 'ratio':v/total[k]} for k,v in removed_by_cuisine.items()]

    # total
    total_removed = 0
    total_total = 0
    with open(get_output_filepath('removed_from_data.csv'), 'w') as f:
        writer = csv.writer(f, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
        writer.writerow(['cuisine', 'removed', 'total', 'ratio'])
        removed_data = sorted(ratio_dict, key=lambda x: x['ratio'], reverse=True)
        for rd in removed_data:
            writer.writerow([rd['cuisine'], rd['remove'], rd['total'], rd['ratio']])
            total_removed += rd['remove']
            total_total += rd['total']
        writer.writerow(['all', total_removed, total_total, total_removed/total_total])

    print('  Total removed: {}'.format(total_removed))
    print('  Total ratio: {:.3f}%'.format((total_removed/total_total)*100))


    # -------------------------------------------
    # Look after few usage of ingredients

    # the complete smaller dataset we validate before (not only the count of ingredients)
    smaller_dataset = [e for e in get_data() if condition(e)]

    # count ingredient usage in each recipe
    ingredient_usage = {}
    # search
    for g in smaller_dataset:
        for i in g['ingredients']:
            # each ingredient count
            try:
                ingredient_usage[i] += 1
            except KeyError:
                ingredient_usage[i] = 1

    # each elements group number count
    ingredient_usage_groups = {}
    # proceed
    for _,v in ingredient_usage.items():
        # each number of ingredient count
        try:
            ingredient_usage_groups[v] += 1
        except KeyError:
            ingredient_usage_groups[v] = 1

    # Plot this
    _, ax = plt.subplots()
    width = 100
    ax.set_title('Bar number of unique usage of ingredients group by number of usage')
    ax.bar(np.array(list(ingredient_usage_groups.keys())), np.array(list(ingredient_usage_groups.values())), width, log=True)
    ax.set_ylabel('number of unique usage')
    plt.savefig(get_output_filepath('ingredients_usage.png'))

    # only 'salt' is >10k usage, remove it
    # what about the distribution?

    # Stat this wihtout salt
    reduced_ingredient_salt_off = {k:v for k,v in ingredient_usage.items() if k != 'salt'}
    print('Number of uniques ingredients without salt: {}, reduced by {} ({:.3f}%)'.format(
        len(reduced_ingredient_salt_off),
        len(ingredient_usage)-len(reduced_ingredient_salt_off),
        ((len(ingredient_usage)-len(reduced_ingredient_salt_off))/len(ingredient_usage))*100
    ))
    ingredient_stats = Stats(reduced_ingredient_salt_off.values())
    ingredient_stats.save(to_file=get_output_filepath('nb_usage_salt_off.csv'))
    ingredient_stats.boxplot(to_file=get_output_filepath('boxplot_nb_usage_salt_off.png'), log=True)

    # Stat this
    reduced_ingredient_usage = {k:v for k,v in ingredient_usage.items() if v > 1 and v < 100}
    print('Number of uniques ingredients without usage >=100 and <=1: {}, reduced by {} ({:.3f}%)'.format(
        len(reduced_ingredient_usage),
        len(ingredient_usage)-len(reduced_ingredient_usage),
        ((len(ingredient_usage)-len(reduced_ingredient_usage))/len(ingredient_usage))*100
    ))
    ingredient_stats = Stats(reduced_ingredient_usage.values())
    ingredient_stats.save(to_file=get_output_filepath('nb_usage_1_100.csv'))
    ingredient_stats.boxplot(to_file=get_output_filepath('boxplot_nb_usage_1_100.png'), log=True)

    # here, we select recipes only if
    # 1/ they valid the first condition
    # 2/ they have at least one ingredient of the reduced list of usages
    condition_v2 = lambda e: condition(e) and set(e['ingredients']) & set(reduced_ingredient_usage) != set()

    #
    working = [r for r in smaller_dataset if condition_v2(r)]

    print('Number of recipes that match the constraint: {}'.format(len(working)))


    # write sorted list
    with open(get_output_filepath('ingredient_usage.txt'), 'w') as f:
        f.write('ingredient,nb of use\n')
        for k,v in sorted(ingredient_usage.items(), key=lambda x: x[1]):
            f.write('{},{}\n'.format(k,v))
