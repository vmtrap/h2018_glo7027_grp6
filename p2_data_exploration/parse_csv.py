
import os.path
import pandas as pd

if __name__ == "__main__":

    csvfile = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'output', 'cleaned_data.csv')
    df = pd.read_csv(csvfile, sep=';')
    print(df.head())

    grouped_count = df.groupby("cuisine").salt.count()
