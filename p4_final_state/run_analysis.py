import csv
import numpy as np
import pandas as pd

# helper
from sklearn.model_selection import StratifiedShuffleSplit

# models
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression, LogisticRegressionCV
from sklearn.multiclass import OneVsRestClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.linear_model import SGDClassifier
from sklearn.linear_model import PassiveAggressiveClassifier
from sklearn.svm import LinearSVC

# metrics
from sklearn.metrics import accuracy_score, confusion_matrix, cohen_kappa_score

# plots
import itertools
import matplotlib.pyplot as plt

# custom
from utils import Utils
from recipe import Recipe, RecipeSet


# Some tool functions
def get_data(filepath):
    """Read data file and return it as dict"""
    with open(filepath, 'r') as f:
        for r in csv.DictReader(f, delimiter=';'):
            yield r

def train_and_test(classifier, x_train, y_train, x_test, y_test):
    """Train a classifier based on train set (X and Y) and print accuracy score based on a test set (X and Y)"""
    print('Training classifier...')
    classifier.fit(x_train, y_train)
    print('Training complete, testing predictions')
    predictions = classifier.predict(x_test)
    print("Score: {:.4f}".format(accuracy_score(y_test, predictions)))


if __name__ == "__main__":

    print("### Data loading\n")
    filepath = Utils.get_output_filepath('rs_train_cleaned_and_filtered_data.csv')

    # Force 0 and 1 in csv file to be int
    to_int = lambda x: int(x)

    # get ingredients
    ingredients = None
    with open(filepath, 'r') as f:
        ingredients = next(f)[:-1].split(';')[2:] # -1 because of last char which is [-605;1H

    # define converter for df values
    converter = {k:to_int for k in ingredients}

    # load from csv and convert data apply converters on all columns of df except 'id' and 'cuisine'
    data = pd.read_csv(filepath, sep=';', index_col='id', converters=converter)

    # Spliting learning variables (X) and truth variable (Y)
    X_mat = data.drop('cuisine', axis = 1).values
    Y_class = data.cuisine.values


    print("### Preliminar tests for classifier choice (each runned 10 times for an average score)\n")

    # Splitting with 30/70 with respect of proportion by class
    sss = StratifiedShuffleSplit(n_splits=10, test_size=0.3)
    for train_index, test_index in sss.split(X_mat , Y_class):

        x_train, y_train = X_mat[train_index], Y_class[train_index]
        x_test, y_test = X_mat[test_index], Y_class[test_index]

        # # Random forest (100 forest)
        print("Random forest\n")
        rfc = RandomForestClassifier(n_estimators = 100, n_jobs = -1)
        train_and_test(rfc, x_train, y_train, x_test, y_test)

        # Logistic regression
        print("Logistic Regression\n")
        lrc = LogisticRegression(n_jobs = -1)
        train_and_test(lrc, x_train, y_train, x_test, y_test)

        # One vs One with SGDC binary classifier
        print("One vs One + SGDC\n")
        ovr = OneVsRestClassifier(SGDClassifier())
        train_and_test(ovr, x_train, y_train, x_test, y_test)

        # Neural network
        print("ANN\n")
        mlpc = MLPClassifier()
        train_and_test(mlpc, x_train, y_train, x_test, y_test)

        # Others classifiers for fun

        # # Passive aggressive
        # print("Passive aggressive\n")
        # pac = PassiveAggressiveClassifier()
        # train_and_test(pac, x_train, y_train, x_test, y_test)

        # # Linear SVM
        # print("SVM\n")
        # lsvc = LinearSVC(multi_class="crammer_singer")
        # train_and_test(lsvc, x_train, y_train, x_test, y_test)

    # Selected model => LogisticRegression


    # print("### Feature extraction")
    # Making some learning on ingredients text corpus could be used to ponderate the impact of each ingredient_usage
    # Idea based on this Kaggle kernel : https://github.com/dmcgarry/kaggle_cooking/
    # However ingredients name doesn't have much "less meaningful information" like "the" "a" "is"


    print("### Feature selection")

    # Selection Kbest features : here 10% most classifying features
    from sklearn.feature_selection import SelectPercentile, chi2
    fit_Pct = SelectPercentile(chi2).fit(X_mat, Y_class)
    X_new = fit_Pct.fit_transform(X_mat, Y_class)



    print("### Spliting training data set into test set and learning set\n")

    nb_splits = 10
    sss = StratifiedShuffleSplit(n_splits=nb_splits, test_size=0.3)
    # for learning_index, test_index in sss.split(X_train , Y_class): # If feature selection was working
    list_models = []
    for learning_index, test_index in sss.split(X_new , Y_class):
        x_learning, y_learning = X_new[learning_index], Y_class[learning_index]
        x_test, y_test = X_new[test_index], Y_class[test_index]

        print("\t### Sub-spliting learning set into learning subset and test subset\n")
        lrc = LogisticRegressionCV(cv = StratifiedShuffleSplit(n_splits=nb_splits, test_size=0.3), max_iter = 100, n_jobs = 10, multi_class = 'multinomial')
        lrc.fit(x_learning, y_learning)
        list_models.append(lrc)

    list_evaluated_models = []
    for model in list_models:
        # Computing performance metrics
        accuracy = accuracy_score(y_test, model.predict(x_test))
        c_kappa = cohen_kappa_score(y_test, model.predict(x_test))
        conf_mat = confusion_matrix(y_test, model.predict(x_test))
        list_evaluated_models.append([model,(accuracy, c_kappa, conf_mat)])

    print("### Selecting best model and exploring classification behaviour\n")
    max_score = 0
    for model_entry in list_evaluated_models:
        tmp_score = model_entry[1][0] + model_entry[1][1]
        if tmp_score > max_score:
            max_score = tmp_score
            final_model = model_entry

    # Plotting confusion matrix (function source : http://scikit-learn.org/stable/auto_examples/model_selection/plot_confusion_matrix.html)
    def plot_confusion_matrix(cm, classes,
                          normalize=False,
                          title='Confusion matrix',
                          cmap=plt.cm.Blues):
        """
        This function prints and plots the confusion matrix.
        Normalization can be applied by setting `normalize=True`.
        """
        if normalize:
            cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
            print("Normalized confusion matrix")
        else:
            print('Confusion matrix, without normalization')

        print(cm)

        plt.imshow(cm, interpolation='nearest', cmap=cmap)
        plt.title(title)
        plt.colorbar()
        tick_marks = np.arange(len(classes))
        plt.xticks(tick_marks, classes, rotation=45)
        plt.yticks(tick_marks, classes)

        fmt = '.2f' if normalize else 'd'
        thresh = cm.max() / 2.
        for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
            plt.text(j, i, format(cm[i, j], fmt),
                     horizontalalignment="center",
                     color="white" if cm[i, j] > thresh else "black")

        plt.tight_layout()
        plt.ylabel('True label')
        plt.xlabel('Predicted label')

    plt.figure(figsize=(12,12))
    plot_confusion_matrix(final_model[1][2], classes = np.unique(Y_class), title='Confusion matrix', normalize=True)
    plt.savefig("output/confusion_matrix.png")

    print("### Predicting on kaggle test set\n")
    filepath = Utils.get_output_filepath('rs_test_cleaned_data.csv')
    # Loading test dataset and putting it in same format as model (same variables)
    test_data = pd.read_csv(filepath, sep=';', index_col='id', converters=converter)

    test_data_formated = pd.DataFrame(0, index=test_data.index, columns=data.columns) # shape of final dataframe, filled with 0
    test_data_formated[test_data.columns & data.columns] = test_data[test_data.columns & data.columns]
    test_data_formated = test_data_formated.drop('cuisine', axis = 1)
    test_data_formated_trunc =
    res_predic = final_model[0].predict(test_data_formated.values)

    print(res_predic)
    pd.DataFrame({'cuisine':res_predic}, index = test_data_formated.index).to_csv("output/final_prediction.csv", sep=",")
