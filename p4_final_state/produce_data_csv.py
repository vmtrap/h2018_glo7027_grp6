
# pip
import re
import os.path
from time import time
from itertools import groupby

# utils
from utils import Utils, StringCompareUtils

# model
from recipe import Recipe, RecipeSet, RecipeTest, RecipeSetTest


if __name__ == "__main__":

    # Getting output folder
    output_dir = Utils.get_output_dir()


    # ------------- TRAIN -------------

    print("### Loading train data ###")
    rs = RecipeSet.from_json('Train dataset', Utils.get_train_dataset())
    print(rs)

    print("### Saving formated training set ###")
    rs.to_csv(Utils.get_output_filepath('rs_train_raw_data.csv'))

    print('### Cleaning ingredients name syntax ###')
    def clean_name(r):
        new_ingredients = []
        for i in r.ingredients:
            tmp = i.lower()
            # remove every s at the end of the words
            tmp = ''.join([i[:-1] if i[-1] == 's' else i for i in tmp.split()])
            # remove useless char
            tmp = tmp.translate(StringCompareUtils.global_table())
            # replace double or more letters by single letter (pleeease -> please)
            tmp = ''.join(ch for ch, _ in groupby(tmp))
            new_ingredients.append(tmp)
        r.ingredients = new_ingredients
        return r

    rs_syntaxe_cleaned = RecipeSet('Syntax cleaned', (clean_name(r) for r in rs.data))


    print("### Searching for similarity in ingredients name ###")
    # Formating "similar" ingredients name so they're the same
    def rename_ingredient(recipe, same_values_list):
        new_ingredients = []
        for i in recipe.ingredients:
            found = None
            # try to find one value in same_value list
            for s in same_values_list:
                if i in s:
                    # print('fusion "{}" -> "{}"'.format(s[1], s[0]))
                    found = s[0] # take the first one
                    break
            # add values in new ingredient list
            new_ingredients.append(i if found is None else found)
        recipe.ingredients = new_ingredients
        return recipe


    # Sampling for levenstein error rate join against word length
    def levenshtein_exact_len(rs, dist_max :int=1, threshold :int=5, transform=None):
        unique_ingredients = rs.unique_ingredients()
        same = []
        for i in range(len(unique_ingredients)):
          str_i = unique_ingredients[i]

          # limit by threshold
          if len(str_i) != threshold:
              continue

          for j in range(i):
              str_j = unique_ingredients[j]

              # limit by threshold
              if len(str_j) != threshold-dist_max and len(str_j) != threshold+dist_max and len(str_j) != threshold:
                  continue

              # compare distance between
              a, b = (str_i, str_j) if transform is None else (transform(str_i), transform(str_j))
              _v = lv.distance(a, b)
              if _v <= dist_max:
                  same.append((a, b))

        return same

    import random
    import Levenshtein as lv
    list_samples = []
    for word_len in range(2,21): # after 21, it tends to have no more than 2 or 3 occurences
        same_values = levenshtein_exact_len(rs_syntaxe_cleaned, threshold = word_len)
        if len(same_values) > 20:
            rand_samples = [ same_values[i] for i in random.sample(range(len(same_values)), 20) ]
        else:
            rand_samples = same_values
        list_samples.append(rand_samples)
        print(rand_samples, "\n")


    # Looking for values that differ only of few letters
    same_values = rs_syntaxe_cleaned.levenshtein(threshold=6)
    rs_cleaned = RecipeSet('Renamed elements', (rename_ingredient(r, same_values) for r in rs_syntaxe_cleaned.data))

    print("### Saving cleaned training set ###")
    rs_cleaned.to_csv(Utils.get_output_filepath('rs_train_cleaned_data.csv'))


    print('### Filtering ###')
    # Based on statistics and graphs made with R's scripts
    print('[Condition 1: 2 < nb ingredients <= 20]')
    condition_nb_ing = lambda e: len(e.ingredients) <= 20 and len(e.ingredients) > 2 # e = one recipe object, condition = the number of ingredients
    rs_filtered_c1 = rs.filter(condition_nb_ing)
    rs_filtered_c1.name = 'C1'
    print(rs_filtered_c1)

    print('[Condition 2: keep recipes where a same ingredient is used in 2 to 100 recipes]')
    reduced_ingredient_usage = {k for k, v in rs_filtered_c1.ingredient_usage().items() if v > 1 and v <= 100}
    condition_nb_use_ing = lambda e: set(e.ingredients) & reduced_ingredient_usage != set()
    rs_filtered_c1c2 = rs_filtered_c1.filter(condition_nb_use_ing)
    rs_filtered_c1c2.name = 'C1+C2'
    print(rs_filtered_c1c2)

    print('[Difference between the two last datasets]')
    print(rs_filtered_c1c2 - rs_filtered_c1)

    rs_final = rs_filtered_c1c2


    print("### Saving formated and cleaned training set ###")
    rs_final.to_csv(Utils.get_output_filepath('rs_train_cleaned_and_filtered_data.csv'))

    # ------------- TEST -------------

    print("### Loading train data ###")
    rs_test = RecipeSetTest.from_json('Test dataset', Utils.get_test_dataset())

    print("### Saving formated test set ###")
    rs_test.to_csv(Utils.get_output_filepath('rs_test_raw_data.csv'))

    print("### Applying cleaning on test set to be comparible")
    rs_test_cleaned = RecipeSetTest('Test dataset cleaned', (clean_name(r) for r in rs_test.data))

    print("### Saving formated and cleaned test set ###")
    rs_test_cleaned.to_csv(Utils.get_output_filepath('rs_test_cleaned_data.csv'))
