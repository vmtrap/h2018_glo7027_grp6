# README

* recipe.py : classes for data definition
* utils.py : classes for data manipulation and correction
* produce_data_csv.py : transform json data to a filtered and cleaned csv matrix used for analysis
* run_analysis.py : main file for running analysis
