
import csv
import numpy as np
import pandas as pd

# helper
from sklearn.model_selection import StratifiedShuffleSplit

# models
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression, LogisticRegressionCV
from sklearn.multiclass import OneVsRestClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.linear_model import SGDClassifier
from sklearn.linear_model import PassiveAggressiveClassifier
from sklearn.svm import LinearSVC

# metrics
from sklearn.metrics import accuracy_score

from utils import Utils
from recipe import Recipe, RecipeSet

def get_data(filepath):
    with open(filepath, 'r') as f:
        for r in csv.DictReader(f, delimiter=';'):
            yield r

def train_and_test(classifier, x_train, y_train, x_test, y_test):
    print('Training classifier...')
    classifier.fit(x_train, y_train)
    print('Training complete, testing predictions')
    predictions = classifier.predict(x_test)
    print("Score: {:.4f}".format(accuracy_score(y_test, predictions)))


if __name__ == "__main__":

    print("### Data loading\n")
    # output folder
    # output_dir = Utils.get_output_dir()
    filepath = Utils.get_output_filepath('rs5_data.csv')

    # define converter for df values
    to_int = lambda x: int(x)

    # get ingredients
    ingredients = None
    with open(filepath, 'r') as f:
        ingredients = next(f)[:-1].split(';')[2:]

    # apply converters on all columns of df except 'id' and 'cuisine'
    converter = {k:to_int for k in ingredients}

    # load from csv and convert data
    data = pd.read_csv(filepath, sep=';', index_col='id', converters=converter)


    print("### Preliminar tests for classifier choice (each runned 5 times for an average score)\n")

    # Spliting learning variables (X) and truth variable (Y)
    X_mat = data.drop('cuisine', axis = 1).values
    Y_class = data.cuisine.values

    # Splitting with 30/70 with respect of proportion by class
    sss = StratifiedShuffleSplit(n_splits=10, test_size=0.3)
    for train_index, test_index in sss.split(X_mat , Y_class):

        x_train, y_train = X_mat[train_index], Y_class[train_index]
        x_test, y_test = X_mat[test_index], Y_class[test_index]

        # # Random forest (100 forest)
        print("Random forest\n")
        rfc = RandomForestClassifier(n_estimators = 100, n_jobs = -1)
        train_and_test(rfc, x_train, y_train, x_test, y_test)

        # Logistic regression
        print("Logistic Regression\n")
        lrc = LogisticRegression(n_jobs = -1)
        train_and_test(lrc, x_train, y_train, x_test, y_test)

        # One vs One with SGDC binary classifier
        print("One vs One + SGDC\n")
        ovr = OneVsRestClassifier(SGDClassifier())
        train_and_test(ovr, x_train, y_train, x_test, y_test)

        # Neural network
        print("ANN\n")
        mlpc = MLPClassifier()
        train_and_test(mlpc, x_train, y_train, x_test, y_test)

        # Others classifiers for fun

        # Passive aggressive
        # print("Passive aggressive\n")
        # pac = PassiveAggressiveClassifier()
        # train_and_test(pac, x_train, y_train, x_test, y_test)

        # Linear SVM
        # print("SVM\n")
        # lsvc = LinearSVC(multi_class="crammer_singer")
        # train_and_test(lsvc, x_train, y_train, x_test, y_test)

    # Selected model => LogisticRegression


    # print("### Feature extraction")
    # Couldn't find something not too complex to implement (maybe for p4 project ?)
    # Making some learning on ingredients text corpus could be used to ponderate the impact of each ingredient_usage
    # Idea based on this Kaggle kernel : https://github.com/dmcgarry/kaggle_cooking/


    # print("### Feature selection")
    # Selection Kbest features : here 10% most classifying features (adapted from http://scikit-learn.org/stable/auto_examples/text/document_classification_20newsgroups.html)
    # from sklearn.feature_selection import SelectKBest, chi2
    # ch2 = SelectKBest(chi2, k=len(X_mat[1]))
    # X_train = ch2.fit_transform(X_mat, Y_class)



    print("### Spliting training data set into test set and learning set\n")

    best_model = None
    nb_splits = 10
    sss = StratifiedShuffleSplit(n_splits=nb_splits, test_size=0.3)
    # for learning_index, test_index in sss.split(X_train , Y_class): # If feature selection was working
    for learning_index, test_index in sss.split(X_mat , Y_class):
        x_learning, y_learning = X_mat[learning_index], Y_class[learning_index]
        x_test, y_test = X_mat[test_index], Y_class[test_index]

        print("\t### Sub-spliting learning set into learning subset and test subset\n")
        lrc_3 = LogisticRegressionCV(cv = StratifiedShuffleSplit(n_splits=nb_splits, test_size=0.3), max_iter = 3, n_jobs = 10)
        lrc_3.fit(x_learning, y_learning)
        lrc_5 = LogisticRegressionCV(cv = StratifiedShuffleSplit(n_splits=nb_splits, test_size=0.3), max_iter = 5, n_jobs = 10)
        lrc_5.fit(x_learning, y_learning)
        lrc_10 = LogisticRegressionCV(cv = StratifiedShuffleSplit(n_splits=nb_splits, test_size=0.3), max_iter = 10, n_jobs = 10)
        lrc_10.fit(x_learning, y_learning)

        max_accu = 0
        for model in [lrc_3, lrc_5, lrc_10]:
            if accuracy_score(y_test, model.predict(x_test)) > max_accu:
                max_accu = accuracy_score(y_test, model.predict(x_test))
                best_model = model


    print("### Predicting on kaggle test set\n")
    filepath = Utils.get_output_filepath('testPredict_data.csv')
    # Loading test dataset and putting it in same format as model (same variables)
    test_data = pd.read_csv(filepath, sep=';', index_col='id', converters=converter)

    test_data_formated = pd.DataFrame(0, index=test_data.index, columns=data.columns) # shape of final dataframe, filled with 0
    test_data_formated[test_data.columns & data.columns] = test_data[test_data.columns & data.columns]
    test_data_formated = test_data_formated.drop('cuisine', axis = 1)
    res_predic = best_model.predict(test_data_formated.values)

    print(res_predic)
    pd.DataFrame({'cuisine':res_predic}, index = test_data_formated.index).to_csv("output/final_prediction.csv", sep=",")
