
import csv
import json
from tqdm import tqdm
import Levenshtein as lv

class Recipe:
    def __init__(self, d):
        if isinstance(d, Recipe):
            self.id = d.id
            self.cuisine = d.cuisine
            self.ingredients = d.ingredients
        else:
            if not isinstance(d, dict):
                raise TypeError
            if not isinstance(d['ingredients'], list):
                raise TypeError
            self.id = d['id']
            self.cuisine = d['cuisine']
            self.ingredients = d['ingredients']

    def to_dict(self):
        return {'id':self.id, 'cuisine':self.cuisine, 'ingredients':self.ingredients}

    def __len__(self):
        return len(self.ingredients)

    def __str__(self):
        return 'recipe: {}, cuisine: {}'.format(self.id, self.cuisine)

class RecipeTest:
    def __init__(self, d):
        if isinstance(d, RecipeTest):
            self.id = d.id
            self.ingredients = d.ingredients
        else:
            if not isinstance(d, dict):
                raise TypeError
            if not isinstance(d['ingredients'], list):
                raise TypeError
            self.id = d['id']
            self.ingredients = d['ingredients']

    def to_dict(self):
        return {'id':self.id, 'ingredients':self.ingredients}

    def __len__(self):
        return len(self.ingredients)

    def __str__(self):
        return 'recipe: {}'.format(self.id)


class RecipeSet:

    """ Recipe dataset

    Expected data [{'id':str, 'cuisine':str, 'ingredients':[]}]
    """

    def __init__(self, name, d):
        self.name = name
        self.data = [Recipe(e) for e in d]
        self._init_index()
        self._init_count()

    def _init_index(self):
        self._index = {r.id:i for i,r in enumerate(self.data)}

    def _init_count(self):
        self.ingredients = None
        self.cuisines = None
        self.nb_recipes = None

    def count_unique_recipes(self):
        if self.nb_recipes is None:
            self.nb_recipes = len(self.data)
        return self.nb_recipes

    # Reprise de l'ancien script
    def unique_ingredients(self):
        if self.ingredients is None:
            _t = set()
            for recipe in self.data:
                for i in recipe.ingredients:
                    _t.add(i)
            self.ingredients = list(_t)
        return self.ingredients

    def get_ingredients(self, normalized=False):
        return {RecipeSet.clean_name(i) if normalized else i for recipe in self.data for i in recipe.ingredients}

    @staticmethod
    def clean_name(ingredient):
        tmp = ingredient.lower()
        # remove every s at the end of the words
        tmp = ''.join([i[:-1] if i[-1] == 's' else i for i in tmp.split()])
        # remove useless char
        tmp = tmp.translate(StringCompareUtils.global_table())
        # replace double or more letters by single letter (pleeease -> please)
        tmp = ''.join(ch for ch, _ in groupby(tmp))
        return tmp

    def get_cuisines(self):
        return {recipe.cuisine for recipe in self.data}

    def update_cuisines(self):
        self.cuisines = list(self.get_cuisines())

    def update_ingredients(self):
        self.ingredients = list(self.get_ingredients())

    def count_unique_ingredients(self):
        try:
            return len(self.ingredients)
        except TypeError:
            self.update_ingredients()
            return len(self.ingredients)

    def count_unique_cuisines(self):
        try:
            return len(self.cuisines)
        except TypeError:
            self.update_cuisines()
            return len(self.cuisines)

    def ingredient_usage(self):
        # count ingredient usage in each recipe
        ingredient_usage = {}
        for g in self.data:
            for i in g.ingredients:
                # each ingredient count
                try:
                    ingredient_usage[i] += 1
                except KeyError:
                    ingredient_usage[i] = 1
        return ingredient_usage

    def filter(self, condition, produce_new_recipeset=True):
        res = list(filter(condition, self.data))
        if produce_new_recipeset:
            return RecipeSet('Filtered "{}"'.format(self.name), res)
        else:
            self.data = res
            self._init_count()
            self._init_index()

    def write_ingredients(self, to_file=None):
        # all unique ingredients
        _t = list(sorted({i for recipe in self.data for i in recipe.ingredients}))
        if to_file is None:
            for i in _t:
                print(i)
        else:
            with open(to_file, 'w') as f:
                for i in _t:
                    f.write('{}\n'.format(i))

    @staticmethod
    def from_json(recipeset_name, from_file):
        def _read():
            with open(from_file, 'r') as f:
                yield from json.load(f, encoding='utf-8')
        return RecipeSet(recipeset_name, _read())

    @staticmethod
    def from_csv(recipeset_name, from_file):
        def _read():
            with open(from_file, 'r') as csvfile:
                for r in csv.DictReader(csvfile, delimiter=';'):
                    yield Recipe({
                        'id':r['id'],
                        'cuisine':r['cuisine'],
                        'ingredients':list(filter(lambda x: x != 'id' and x != 'cuisine' and r[x] == '1', r))
                    })
        return RecipeSet(recipeset_name, _read())

    def to_json(self, to_file=None, verbose=False):
        if to_file is None:
            for r in self.data:
                print(r)
        else:
            with open(to_file, 'w') as f:
                json.dump([r.to_dict() for r in self.data], f)
            if verbose:
                print('[{}] written to "{}"'.format(self.name, to_file))

    def to_csv(self, to_file=None, verbose=False):
        if to_file is None:
            for r in self.data:
                print(r)
        else:
            # produce file
            ui = self.unique_ingredients()
            with open(to_file, 'w') as f:
                writer = csv.writer(f, delimiter=';', quotechar='|', quoting=csv.QUOTE_MINIMAL)
                writer.writerow(['id', 'cuisine'] + ui)
                for d in tqdm(list(self.data)):
                    writer.writerow([d.id, d.cuisine] + [1 if i in d.ingredients else 0 for i in ui])

    def get_tests(self):
        """Get only the list of all ingredients by recipe
        Useful for ML test set
        """
        return [r.ingredients for r in self.data]

    def get_answers(self):
        """Get only the cuisine type by recipe
        Useful for ML answer set
        """
        return [r.cuisine for r in self.data]

    def get_train_test(self):
        return [(r.ingredients, r.cuisine) for r in self.data]

    def levenshtein(self, threshold :int=5, transform=None):
        unique_ingredients = self.unique_ingredients()
        same = []
        for i in range(len(unique_ingredients)):
            str_i = unique_ingredients[i]

            # limit by threshold
            if len(str_i) < threshold:
                continue

            for j in range(i):
                str_j = unique_ingredients[j]

                # limit by threshold
                if len(str_j) < threshold:
                    continue

                # compare distance between
                a, b = (str_i, str_j) if transform is None else (transform(str_i), transform(str_j))
                _v = lv.distance(a, b)
                if _v == 0:
                    same.append((a, b))

        return same

    def __len__(self):
        return self.count_unique_recipes()

    def __str__(self):
        return '[{}] recipes: {}, unique cuisines: {}, unique ingredients: {}'.format(
            self.name, self.count_unique_recipes(), self.count_unique_cuisines(),
            self.count_unique_ingredients())

    def __contains__(self, recipe):
        if not isinstance(recipe, Recipe):
            raise TypeError
        return recipe.id in self._index

    def __sub__(self, other):
        if not isinstance(other, RecipeSet):
            raise TypeError
        self._init_index()
        new_name = '({}) - ({})'.format(self.name, other.name)
        d1, d2 = (self, other) if len(self) > len(other) else (other, self)
        return RecipeSet(new_name, (r for r in d1.data if r not in d2))

class RecipeSetTest(RecipeSet):

    def __init__(self, name, d):
        self.name = name
        self.data = [RecipeTest(e) for e in d]
        self._init_index()
        self._init_count()

    def _init_index(self):
        self._index = {r.id:i for i,r in enumerate(self.data)}

    def _init_count(self):
        self.ingredients = None
        self.nb_recipes = None

    def get_answers(self):
        raise NotImplementedError

    def get_answers(self):
        raise NotImplementedError

    def get_cuisines(self):
        raise NotImplementedError

    def update_cuisines(self):
        raise NotImplementedError

    def count_unique_cuisines(self):
        raise NotImplementedError

    def get_train_test(self):
        raise NotImplementedError

    @staticmethod
    def from_json(recipeset_name, from_file):
        def _read():
            with open(from_file, 'r') as f:
                yield from json.load(f, encoding='utf-8')
        return RecipeSetTest(recipeset_name, _read())

    @staticmethod
    def from_csv(recipeset_name, from_file):
        def _read():
            with open(from_file, 'r') as csvfile:
                for r in csv.DictReader(csvfile, delimiter=';'):
                    yield RecipeTest({
                        'id':r['id'],
                        'ingredients':list(filter(lambda x: x != 'id' and x != 'cuisine' and r[x] == '1', r))
                    })
        return RecipeSetTest(recipeset_name, _read())

    def to_json(self, to_file=None, verbose=False):
        raise NotImplementedError

    def to_csv(self, to_file=None, verbose=False):
        if to_file is None:
            for r in self.data:
                print(r)
        else:
            # produce file
            ui = self.unique_ingredients()
            with open(to_file, 'w') as f:
                writer = csv.writer(f, delimiter=';', quotechar='|', quoting=csv.QUOTE_MINIMAL)
                writer.writerow(['id'] + ui)
                for d in tqdm(list(self.data)):
                    writer.writerow([d.id] + [1 if i in d.ingredients else 0 for i in ui])

    def __len__(self):
        return self.count_unique_recipes()

    def __str__(self):
        return '[{}] recipes: {}, unique ingredients: {}'.format(
            self.name, self.count_unique_recipes(),
            self.count_unique_ingredients())

    def __contains__(self, recipe):
        if not isinstance(recipe, Recipe):
            raise TypeError
        return recipe.id in self._index

    def __sub__(self, other):
        raise NotImplementedError
