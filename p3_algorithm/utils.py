
import os
import os.path

class Utils:

    @staticmethod
    def get_data():
        """Yield each cooking informations"""
        d = os.path.join(os.path.dirname(os.path.dirname(
            os.path.abspath(__file__))), 'data', 'train.json')
        with open(d) as f:
            yield from json.load(f, encoding='utf-8')

    @staticmethod
    def get_output_dir():
        output_dir = os.path.join(os.path.dirname(
            os.path.abspath(__file__)), 'output')
        if not os.path.isdir(output_dir):
            os.mkdir(output_dir)
        return output_dir

    @staticmethod
    def get_output_filepath(filename):
        return os.path.join(Utils.get_output_dir(), filename)

    @staticmethod
    def get_train_dataset():
        return os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), 'data', 'train.json')

    @staticmethod
    def get_test_dataset():
        return os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), 'data', 'test.json')


class StringCompareUtils:

    NULLIFY_SPECIAL_CHAR_TABLE = {
        ord('&'): None,
        ord('-'): None,
        ord('%'): None,
        ord('®'): None,
        ord('™'): None,
        ord('%'): None,
        ord('$'): None,
        ord('€'): None,
        ord('.'): None,
        ord('/'): None,
        ord('\\'): None,
        ord('#'): None,
        ord('!'): None,
        ord('?'): None,
        ord('('): None,
        ord(')'): None,
        ord('’'): None,
        ord("'"): None,
        ord(' '): None,
        ord('0'): None,
        ord('1'): None,
        ord('2'): None,
        ord('3'): None,
        ord('4'): None,
        ord('5'): None,
        ord('6'): None,
        ord('7'): None,
        ord('8'): None,
        ord('9'): None,
    }

    NORMALIZE_SPECIAL_CHAR_TABLE = {
        ord('ú'): 'u',
        ord('ù'): 'u',
        ord('é'): 'e',
        ord('è'): 'e',
        ord('ë'): 'e',
        ord('ê'): 'e',
        ord('ç'): 'c',
        ord('à'): 'a',
        ord('ä'): 'a',
        ord('â'): 'a',
    }

    @staticmethod
    def global_table():
        return {**StringCompareUtils.NULLIFY_SPECIAL_CHAR_TABLE, **StringCompareUtils.NORMALIZE_SPECIAL_CHAR_TABLE}
