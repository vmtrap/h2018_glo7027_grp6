
# pip
import re
import os.path
from time import time
from itertools import groupby

# utils
from utils import Utils, StringCompareUtils

# model
from recipe import Recipe, RecipeSet, RecipeTest, RecipeSetTest


if __name__ == "__main__":

    # output folder
    output_dir = Utils.get_output_dir()

    # base dataset
    rs = RecipeSet.from_json('Train dataset', Utils.get_train_dataset())
    print(rs)

    # our condition, e is one recipe object
    # condition is the number of ingredients
    # condition = lambda e: len(e['ingredients']) <= 20 and len(e['ingredients']) > 2
    condition = lambda e: len(e.ingredients) <= 20 and len(e.ingredients) > 2

    print('\ncondition: 2 < nb ingredient <= 20')
    rs2 = rs.filter(condition)
    rs2.name = 'Condition 1'
    print(rs2)

    # here, we select recipes only if
    # 1/ they valid the first condition
    # 2/ they have at least one ingredient of the reduced list of usages
    reduced_ingredient_usage = {k for k, v in rs2.ingredient_usage().items() if v > 1 and v <= 100}
    condition_v2 = lambda e: condition(e) and set(e.ingredients) & reduced_ingredient_usage != set()

    rs3 = rs.filter(condition_v2)
    print('\ncondition: 2 < nb ingredient < 20 et recette où l\'ingredient est globalement utilisé entre 2 et 100 fois')
    rs3.name = 'Condition 2'
    print(rs3)

    # rs3.write_ingredients(get_output_filepath('rs3_unique_ingredient.txt'))
    # rs3.produce_matrix(get_output_filepath('cleaned_data.csv'))

    print('\nDifference between the two last datasets')
    print(rs3 - rs2)

    print('\nSearching for similarity in ingredients name...')

    # # without transformations ~20s
    # start_time = time()
    # rs3.levenshtein()
    # print('Elaspsed: {:.3f}s'.format(time() - start_time))

    # with transformations ~6min
    def clean(d):
        tmp = d.lower()
        # remove every s at the end of the words
        tmp = ''.join([i[:-1] if i[-1] == 's' else i for i in tmp.split()])
        # remove useless char
        tmp = tmp.translate(StringCompareUtils.global_table())
        # replace double or more letters by single letter (pleeease -> please)
        tmp = ''.join(ch for ch, _ in groupby(tmp))
        return tmp

    start_time = time()
    same_values = rs3.levenshtein(transform=clean)
    print('Elaspsed: {:.3f}s, {} changes to apply'.format(time() - start_time, len(same_values)))

    print('Performing changes... ', end='')

    def rename_ingredient(recipe, same_values_list):
        new_ingredients = []
        for i in recipe.ingredients:
            found = None
            # try to find one value in same_value list
            for s in same_values_list:
                if i in s:
                    # print('fusion "{}" -> "{}"'.format(s[1], s[0]))
                    found = s[0] # take the first one
                    break
            # add values in new ingredient list
            new_ingredients.append(i if found is None else found)
        recipe.ingredients = new_ingredients
        return recipe

    # 4th recipe set
    rs4 = RecipeSet('renamed elements from rs3', (rename_ingredient(r, same_values) for r in rs3.data))
    print('done.')
    print(rs4)

    def clean_name(r):
        new_ingredients = []
        for i in r.ingredients:
            tmp = i.lower()
            # remove every s at the end of the words
            tmp = ''.join([i[:-1] if i[-1] == 's' else i for i in tmp.split()])
            # remove useless char
            tmp = tmp.translate(StringCompareUtils.global_table())
            # replace double or more letters by single letter (pleeease -> please)
            tmp = ''.join(ch for ch, _ in groupby(tmp))
            new_ingredients.append(tmp)
        r.ingredients = new_ingredients
        return r

    rs5 = RecipeSet('normalized rs4', (clean_name(r) for r in rs4.data))

    rs5.to_csv(Utils.get_output_filepath('rs5_data.csv'))


    # Test data set production (csv)
    # base dataset
    rs = RecipeSetTest.from_json('Test dataset', Utils.get_test_dataset())
    print(rs)

    def clean_name(r):
        new_ingredients = []
        for i in r.ingredients:
            tmp = i.lower()
            # remove every s at the end of the words
            tmp = ''.join([i[:-1] if i[-1] == 's' else i for i in tmp.split()])
            # remove useless char
            tmp = tmp.translate(StringCompareUtils.global_table())
            # replace double or more letters by single letter (pleeease -> please)
            tmp = ''.join(ch for ch, _ in groupby(tmp))
            new_ingredients.append(tmp)
        r.ingredients = new_ingredients
        return r

    rs2 = RecipeSetTest('Test dataset 2', (clean_name(r) for r in rs.data))

    rs2.to_csv(Utils.get_output_filepath('testPredict_data.csv'))
