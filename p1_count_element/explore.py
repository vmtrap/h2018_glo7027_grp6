#!/usr/bin/env python3
# -*- coding: utf-8 -*

import os.path
import json

def get_data():
    """Yield each cooking informations"""
    d = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), 'data', 'train.json')
    with open(d) as f:
        yield from json.load(f, encoding='utf-8')

def get_output_dir():
    """Get the output directory path"""
    output_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'output')
    if not os.path.isdir(output_dir):
        os.mkdir(output_dir)
    return output_dir

def write_txt_file(filepath, iterable_data):
    """Write a txt file line by line with iterable data"""
    with open(filepath, 'w') as f:
        for i in sorted(iterable_data):
            f.write('{}\n'.format(i))

if __name__ == "__main__":

    # config output filepath for write method
    output_dir = get_output_dir()
    get_output_filepath = lambda x: os.path.join(output_dir, x)

    # count records
    nb_rec = sum([1 for _ in get_data()])
    print('{} records'.format(nb_rec))

    # cuisine type
    unique_cuisine = {f['cuisine'] for f in get_data()}
    print('{} unique cuisine'.format(len(unique_cuisine)))

    # write sorted unique cuisine list in file
    write_txt_file(get_output_filepath('unique_cuisine.txt'), unique_cuisine)

    # count unique ingredients
    unique_food = {d for f in get_data() for d in f['ingredients']}
    print('{} unique ingredients'.format(len(unique_food)))

    # write sorted unique ingredient list in file
    write_txt_file(get_output_filepath('unique_ingredient.txt'), unique_food)
